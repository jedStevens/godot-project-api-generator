import sys

def isFloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False

class GDFunc:
	def __init__(self, _name, _ret_t='void', _params=[], _doc=[]):
		self.gd_func_name = _name
		self.gd_return_type = _ret_t
		self.gd_params = _params
		self.gd_doc = _doc

	def add_gd_docline(self, line):
		self.gd_doc.append(line)

	def html(self):
		comment = "" if not len(self.gd_doc) else ('<p class="doc-comment">%s</p><br>'%(''.join(self.gd_doc)))
		params = ''.join([v.html() for v in self.gd_params])
		return f"<div class='code-func-section'><a class='code-type type-{self.gd_return_type}'>{self.gd_return_type}</a><a class='code-func-name'>{self.gd_func_name}</a><div class='params-container'>{params}</div></div>{comment}"

class GDVar:
	def __init__(self, _name, _type, _val=None, line="", _doc=[]):
		self.gd_var_name = _name
		self.gd_var_type = _type.lstrip().rstrip()
		self.default_value = _val
		self.gd_doc=_doc
		self.gd_declare_line=""

		if self.gd_var_type == 'var' and ":" in self.gd_var_name:
			self.gd_var_name,self.gd_var_type = self.gd_var_name.split(":")
		if self.gd_var_type == 'var' and '=' in line:
			self.gd_var_type = self.get_implied_type(line.split("=")[1].strip())
		if "=" in self.gd_var_type and self.default_value == None:
			self.gd_var_type,self.default_value = self.gd_var_type.split("=")
		if self.gd_var_type == 'var' and self.default_value != None:
			self.gd_var_type=self.get_implied_type(self.default_value)
		if self.gd_var_type == 'var' and self.gd_var_name in ['_delta','delta']:
			self.gd_var_type = 'float'
		self.gd_var_name = self.gd_var_name.replace(" ", "")
		self.gd_var_type = self.gd_var_type.replace(" ", "")
	def get_implied_type(self, input):
		if "load" in input:
			if '.instance' in input:
				return 'Node'
			return "PackedScene"
		if input.isdigit():
			return "int"
		if input.startswith("Vector2"):
			return "Vector2"
		if input.startswith("Vector3"):
			return "Vector3"
		if (input[0] == "[" and input[-1]=="]") or 'Array' in input:
			return "Array"
		if (input[0] == "{" and input[-1]=="}"):
			return "Dictionary"
		if isFloat(input):
			return "float"
		if "'" in input or '"' in input:
			return "String"
		return "var"
	def add_gd_docline(self, line):
		self.gd_doc.append(line)
	def html(self):
		doc = "" if (len(self.gd_doc) == 0) else ("<p class='doc-comment'>%s</p><br>"%(''.join(self.gd_doc)))
		val = "" if self.default_value == None else f"<a class='code-var-val'>= {self.default_value}</a>"
		return f"<div class='code-var-section'><a class='code-type type-{self.gd_var_type}'>{self.gd_var_type}</a><a class='code-var-name'>{self.gd_var_name}</a>{val}</div>{doc}"

	def __cmp__(self, other):
		return cmp(self.gd_var_name, other.gd_var_name)


class GDScript:
	def __init__(self):
		self.last_item = self
		self.code=""
		self.gd_class_name="NoNameClass"
		self.tool_flag = False
		self.gd_parent_class="Object"
		self.gd_vars = []
		self.gd_funcs = []
		self.gd_doc = []
		self.doc_stash=[]

	def add_gd_docline(self,line):
		self.gd_doc.append(line)

	def add_gd_var(self, var_name, var_type, var_val=None, line=''):
		self.gd_vars.append(GDVar(var_name, var_type, var_val, line, self.doc_stash))
		self.last_item = self.gd_vars[-1]

	def add_gd_varline(self,line):
		x = line.split(" ")
		self.add_gd_var(x[1], 'var', x[-1], line)

	def add_gd_varlineX(self,line):
		x = line.split(" ")
		y = line.split('=')
		if len(y) > 1:
			self.add_gd_var(y[0].rstrip().split(" ")[-1], x[0][7:-1], y[1].lstrip().split(" ")[0].rstrip(), line)
		else:
			self.add_gd_var(y[0].replace(" ", ""), "var")

	def add_gd_funcline(self, line):
		data = line.split(" ")[1].split("(")
		p_str = line[line.find("(")+1:line.find(")")]
		params = []
		ret = 'void' if not('->' in line) else line.split('->')[1].rstrip()[:-1]
		if p_str != "":
			params = [GDVar(x.lstrip(),'var') for x in p_str.split(",")]
		self.gd_funcs.append(GDFunc(data[0], ret, params, self.doc_stash))
		self.last_item=self.gd_funcs[-1]

	def add_comment(self,comment):
		self.last_item.add_gd_docline(comment[1:])

	def read_lines(self, lines):
		for line in lines:
			self.read_line(line)
	def read_line(self, line):
		if line == "@tool":
			self.tool_flag = True
		elif line.startswith("extends"):
			self.gd_parent_class = line.split(" ")[1]
		elif line.startswith('var'):
			self.add_gd_varline(line)
		elif line.startswith('@export'):
			self.add_gd_varlineX(line)
		elif line.startswith('func'):
			self.add_gd_funcline(line)
		elif line.startswith('class_name'):
			self.gd_class_name = line.split(" ")[1]

		if line.lstrip().startswith("#"):
			self.doc_stash.append(line.lstrip()[1:])
		elif line.lstrip() != '':
			self.doc_stash = []
	def export(self):
		vars = ''.join([v.html() for v in sorted(self.gd_vars, key=lambda x: x.gd_var_name)])
		class_docs = '<br>'.join(x for x in self.gd_doc)
		methods = ''.join(x.html() for x in sorted(self.gd_funcs, key=lambda x:x.gd_func_name))
		return f"""
<h2>{self.gd_class_name}</h2>
<lead>extends {self.gd_parent_class}</lead>

{class_docs}

<h3>Member Variables</h3>
<div class='vars-container'>
{vars}
</div>
<h3>Methods</h3>
<div class='vars-container'>
{methods}
</div>
"""

if __name__ == "__main__":
	script = GDScript()
	script.gd_class_name = sys.argv[1].split("/")[-1].replace(".gd","")
	lines = []
	with open(sys.argv[1], 'r') as f:
		lines = f.readlines()
	script.read_lines(lines)
	print(script.export())

def convert_godot_script(gd_path):
	script = GDScript()
	script.gd_class_name = gd_path.split("/")[-1]
	lines = []
	with open(gd_path, 'r') as f:
		lines = f.readlines()
	script.read_lines(lines)
	return script.export()

