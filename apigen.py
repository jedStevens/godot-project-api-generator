import os
import sys
import glob
import gd2md

def printerr(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


default_css = """CSS ERROR, CSS not loaded from stab folder"""

if __name__ == "__main__":
	print("Running API GEN")

	# Loads stabs from project generator repo
	stabs_path = os.path.join(__file__[0:__file__.rfind("/")], "stabs")
	stab_files = os.listdir(stabs_path)
	stabs = {}
	css_path = ""
	css = ""
	for f in stab_files:
		if f.endswith(".css"):
			css_path =  f
			with open(os.path.join(stabs_path, f)) as css_f: css = css_f.read()
			continue
		with open(os.path.join(stabs_path, f)) as stab_f: stabs[f.split(".")[0]] = stab_f.read()
	
	print("Loaded Stabs [%d]"%len(stabs))
	print("CSS found: ", len(css_path) > 0)
	print("Collecting Files")
	files = files = glob.glob('**/*.gd', recursive=True)
	if len(files) == 0:
		printerr("No Files to Lint; nothing to generate")
		sys.exit(1)
	print("Linting Files [%d]"%len(files))

	print("Loading Default HTML Frame")

	print("Generating index.html")

	#Generic solution
	#Page=[Frame[Sidebar + PageContents] + Footer]
	#PageContents varies depending on type of page being viewed
	# For Now Home/API only 2 pagemodes exist
	html = stabs['page']
	base_url = "/"+os.environ['BASE_API_URL']+"/"
	sidebar_items = [stabs['sidebar-item'].format(URL=base_url+x+".html",TEXT=x.split("/")[-1]) for x in files]
	sidebar = stabs['sidebar'].format(ITEMS="\n".join(sidebar_items))
	print("Generating index.html; Include a public/index.html file to override the generated page manually.")
	for file in files:
		page = gd2md.convert_godot_script(file)
		page_path = os.path.join("public", file+".html")
		os.makedirs(os.path.dirname(page_path), exist_ok=True)
		with open(page_path, 'w') as f:
			f.write(html.format(PAGE=page, SIDEBAR=sidebar, FOOTER="test footer"))
		print("Successful Page: ", file)
	
	print("Done Generating API")
	
	with open("public/style.css", "w") as stylesheet:
		stylesheet.write(css)
	print("Wrote CSS. Done Generating All HTML")

