# Godot Project API Generator

## Description
Generate documentation for Godot Projects and Addons. Have a project you need clean looking documentation for? Use this either locally or within your CI set up to generate documentation automatically. Store all of your documentation within your own code with comments, and this linter will scrape them into nice looking tooltips and description sections.

## Installation and Usage
First go to your project directory (Differs for CI set ups)

`cd ExampleProjectFolder`

These next two commands will first download the API generator, then run it in your project directory

`git clone https://gitlab.com/jedStevens/godot-project-api-generator`

`python godot-project-api-generator/apigen.py public`

The last argument `public` is a target directory; if you need to change this to something else for your set up please do so; but in a GitLab CI set up this will target your pages directory.

Your results will be a generated set of pages that function as an API reference section of a project website.

## Authors and acknowledgment
Jed Stevens

## License
MIT License
